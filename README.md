# Light Phone Radio Stations

## About

Although not its intended use, the Podcasts tool is capable of playing online radio streams. You'll have to craft your own XML and host it elsewhere, however.

## Adding Radio Stations

You can find more than 40,000 stations available for streaming over at [https://www.radio-browser.info/](https://www.radio-browser.info/).

Search for a radio station in the top right search bar. If you don't know the name of a station, you should search by tag:

![Searching on RadioBrowser](https://gitlab.com/ScaryOrca/light-phone-radio-stations/-/raw/main/images/image-1.png)

After you've found a radio station you'd like to add, right click on "Save", and copy the link:

![Copying a radio station's URL to clipboard](https://gitlab.com/ScaryOrca/light-phone-radio-stations/-/raw/main/images/image-2.png)

Once you have your link, you can add a new station to your XML. Your XML might look something like:

```
<?xml version="1.0" encoding="UTF-8"?>
<rss xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:media="http://search.yahoo.com/mrss/" version="2.0">
  <channel>
    <title>Personal</title>
    <description>Warner's Radio Stations</description>
    <language>en-us</language>

    <!-- NPR -->
    <item>
      <title>WPLN</title>
      <enclosure length="0" type="audio/mpeg" url="https://wpln.streamguys1.com/wplnfm.aac"/>
    </item>

    <!-- Dance Wave Retro! -->
    <item>
      <title>Dance Wave Retro!</title>
      <enclosure length="0" type="audio/mpeg" url="http://stream.dancewave.online:8080/retrodance.mp3"/>
    </item>

    <!-- 6forty Radio -->
    <item>
      <title>6forty Radio</title>
      <enclosure length="0" type="audio/mpeg" url="http://radio.6forty.com:8000/6forty"/>
    </item>
  </channel>
</rss>
```